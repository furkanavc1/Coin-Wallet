const btcEl = document.getElementById("btc-el")
const ethEl = document.getElementById("eth-el")
const tronEl = document.getElementById("tron-el")
const btcDiv = document.getElementById("btc-div")
const ethDiv = document.getElementById("eth-div")
const tronDiv = document.getElementById("tron-div")
const depoEl = document.getElementById("deposit-btn")
const withEl = document.getElementById ("withdraw-btn")
const viewCoin = document.getElementById("view-coin")
const transDiv = document.getElementById("trans-div")

let coin = 0

const val = document.getElementById("input-el")
btcDiv.style.display = "none"
ethDiv.style.display = "none"
tronDiv.style.display = "none"



function transaction(price, state){
    const object2 = {
        type: state,
        amount: price
    }
    createItem(object2)
}

function createItem (item) {
    let str = ""
    let clsUp = ""
    let clsDown= ""
    const iconUp = document.createElement("i")
    const iconDown = document.createElement("i")
    if(item.type === "deposit"){
        clsUp = "fas fa-arrow-up text-matrix-green"
        str = ` Deposit +${item.amount}`
    }else{
        clsDown = "fas fa-arrow-down text-red-700"
        str = ` Withdraw -${item.amount}`
    }
    iconUp.className = clsUp
    iconDown.className = clsDown
    const divEl = document.createElement("div")
    const textEl = document.createTextNode(str)
    divEl.className = "cls"
    divEl.appendChild(textEl)
    divEl.appendChild(iconUp)
    divEl.appendChild(iconDown)
    transDiv.appendChild(divEl)
}


depoEl.addEventListener("click", function(){
    state = "deposit"
    const val = document.getElementById("input-el")
    const inputVal = parseInt(val.value)
    
    if (val.value.trim().length){
        coin += inputVal
        viewCoin.innerHTML = coin
        transaction(inputVal, state)
    } else{
        console.log("u need to give deposit amount")
    }
    
})

withEl.addEventListener("click", function(){
    state = "withdraw"
    
    const val = document.getElementById("input-el")
    const inputVal = parseInt(val.value)
    if(coin<inputVal){
        console.log("paran yok")
    }else if(val.value.trim().length){
        transaction(inputVal, state)
        coin -= inputVal
    }else {
        console.log("u need to give withdraw amount")
    }
    viewCoin.innerHTML = coin
})


/////////////////////////////////////////////////////////////////////7

btcEl.addEventListener("click", function(){
    btcDiv.style.display = "block"
    ethEl.style.display = "none"
    tronEl.style.display = "none"
})

ethEl.addEventListener("click", function(){
    ethDiv.style.display = "block"
    btcEl.style.display = "none"
    tronEl.style.display = "none"
})

tronEl.addEventListener("click", function(){
    tronDiv.style.display = "block"
    ethEl.style.display = "none"
    btcEl.style.display = "none"
})